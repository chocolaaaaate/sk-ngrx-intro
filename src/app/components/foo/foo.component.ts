import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-foo',
  templateUrl: './foo.component.html',
  styleUrls: ['./foo.component.scss'],
})
export class FooComponent {
  userText: string;
  constructor(private store: Store<any>) {}

  dispatchUserText() {
    console.log('dispatching');

    this.store.dispatch({
      type: 'TEXT_ENTERED',
      payload: this.userText,
    });
  }
}
