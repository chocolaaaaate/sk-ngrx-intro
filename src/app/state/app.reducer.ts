export interface State {
  text: string;
}

export function reducer(state: State = null, action): State {
  console.log('reducer called: ', 'state: ', state, 'action: ', action);

  switch (action.type) {
    case 'TEXT_ENTERED':
      return {
        ...state,
        text: action.payload,
      };
    default:
      return state;
  }
}
